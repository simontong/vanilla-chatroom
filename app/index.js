"use strict";

let http = require('http'),
  fs = require('fs'),
  View = require('../lib/View');

let server = http.createServer((req, res) => {
  let url = req.url != '/' ? req.url : 'index.html';

  try {
    if (!fs.existsSync(__dirname + `/../public/${url}`)) {
      // get end point
      let url = req.url.match(/^\/([^\/]+)(\/([^\/]+))?/),
        endpoint = url[1],
        action = (url[3] ? url[3] : 'index');

      // prepare view
      let view = new View(`${endpoint}.${action}`);

      // write 200 head for all routes
      res.writeHead(200, {
        'content-type': 'text/html'
      });

      // render view
      view.render().then(content => {
        res.end(content)
      }).catch(e => {
        res.writeHead(404);
        res.end('An error occurred: ' + e);
      });
    }

    else {
      // render static file
      fs.readFile(__dirname + `/../public/${url}`, (err, data) => {
        if (err) throw err;
        res.end(data);
      });
    }
  }
  catch (e) {
    res.writeHead(500);
    res.end('An error occurred: ' + e);
  }

});

server.listen(3000, () => {
  console.log('Server listening on http://localhost:3000');
});

// add to global vars
global.server = server;




