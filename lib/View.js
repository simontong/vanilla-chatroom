let fs = require('fs');

const SCRIPTS_MARKER = '__SCRIPTS';
const CSS_URLS_MARKER = '__CSS_URLS';
const BLOCK_MARKER = '__BLOCK';

class View {
  constructor(view, params = {}) {
    this.view = view;
    this.params = params;
    this.layers = [];
    this.blockBuf = [];
    this.scriptFiles = [];
    this.cssFiles = [];
  }

  extends(view, params = {}) {
    this.addLayer(view, params);
    return '';
  }

  block(name) {
    return `${BLOCK_MARKER}:${name}`;
  }

  beginBlock(name) {
    this.blockBuf.push(name);
    return `${BLOCK_MARKER}_START:${name}`;
  }

  endBlock() {
    return `${BLOCK_MARKER}_END`;
  }

  scripts() {
    return SCRIPTS_MARKER;
  }

  addScript(url) {
    this.scriptFiles.push(url);

    return '';
  }

  cssUrls() {
    return CSS_URLS_MARKER;
  }

  addCssUrl(url) {
    this.cssFiles.push(url);

    return '';
  }

  addLayer(view, params = {}) {
    let viewFile = `${__dirname}/../app/views/${view.replace('.', '/')}.html`;
    let promise = new Promise((resolve, reject) => {

      if (!fs.existsSync(viewFile)) {
        return reject(`File ${viewFile} not found.`);
      }
      let content = fs.readFileSync(viewFile);
      let vars = '';
      Object.keys(params).forEach(function (k) {
        vars += `var ${k}=this.params['${k}'];`;
      });

      let view = new Function(vars + 'return `' + content + '`;');
      view.params = params;

      resolve(view.call(this));
    }).catch(e => {
      throw e;
    });

    this.layers.push(promise);
  }

  render() {
    this.addLayer(this.view, this.params);

    return new Promise((resolve, reject) => {
      Promise.all(this.layers).then(layers => {
        let compiled = layers.join('');

        for (let block of this.blockBuf) {
          let regexps = {
            block: new RegExp(`${BLOCK_MARKER}_START:${block}([\\s\\S]+?)${BLOCK_MARKER}_END`),
            blockMarker: `${BLOCK_MARKER}:${block}`,
            scriptMarker: SCRIPTS_MARKER,
            cssMarker: CSS_URLS_MARKER,
          };

          // blocks ---
          let blockMarker = compiled.match(regexps.block);
          if (blockMarker) {
            compiled = compiled
              .replace(regexps.block, '')
              .replace(regexps.blockMarker, blockMarker[1]);
          }
          else {
            // remove block section markers
            compiled = compiled.replace(regexps.blockMarker, '');
          }

          // scripts ---
          if (compiled.match(regexps.scriptMarker)) {
            let scripts = [];
            for (let script of this.scriptFiles) {
              scripts.push(`<script src="${script}"></script>`);
            }
            compiled = compiled.replace(regexps.scriptMarker, scripts.join("\n"));
          }

          // css ---
          if (compiled.match(regexps.cssMarker)) {
            let cssUrls = [];
            for (let css of this.cssFiles) {
              cssUrls.push(`<link rel="stylesheet" href="${css}"/>`);
            }
            compiled = compiled.replace(regexps.cssMarker, cssUrls.join("\n"));
          }
        }

        resolve(compiled);

      }).catch (e => {
        reject(e);
      });
    });
  }
}

module.exports = View;